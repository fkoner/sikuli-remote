package org.sikuli.remote.client;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import org.sikuli.api.ScreenRegion;
import org.sikuli.api.visual.ImageViewer;
import org.sikuli.api.visual.ScreenRegionCanvas;

import com.google.common.base.Objects;

public class RemoteScreenRegionCanvas extends ScreenRegionCanvas {

	public RemoteScreenRegionCanvas(ScreenRegion screenRegion) {
		super(screenRegion);
	}

	private ImageViewer viewer;

	public void display(int seconds){

		Rectangle r = getScreenRegion().getBounds();

		String title = String.format("RemoteScreenRegion (%d,%d) %dx%d", r.x,r.y,r.width,r.height);

		BufferedImage image = createImage();
		viewer = Objects.firstNonNull(viewer, new ImageViewer());
		viewer.updateImage(image);
		viewer.setTitle(title);
		viewer.setVisible(true);

		try {
			Thread.sleep(seconds*1000);
		} catch (InterruptedException e) {
		}

		viewer.setVisible(false);
	}
}