package org.sikuli.remote.examples;


import java.awt.Color;
import java.net.URL;

import org.sikuli.api.ImageTarget;
import org.sikuli.api.ScreenRegion;
import org.sikuli.api.robot.Mouse;
import org.sikuli.api.visual.Canvas;
import org.sikuli.api.visual.ScreenRegionCanvas;
import org.sikuli.remote.Remote;
import org.sikuli.remote.SikuliRemote;
import org.sikuli.remote.client.RemoteMouse;
import org.sikuli.remote.client.RemoteScreenRegion;

public class RemoteFindExample {

	public static void main(String[] args) throws Exception {
	
		int port = 5000;	
		//String host = "localhost";
//		String host = "10.0.0.6";
		String host = "10.0.0.9";
		
		URL serverUrl = new URL("http://" + host + ":" + port + "/sikuli");
		

		Remote remote = new SikuliRemote(serverUrl);
		
		ScreenRegion s = new RemoteScreenRegion(remote);

		//		URL	imageUrl = new URL("http://code.google.com/images/code_logo.gif");
		//ScreenRegion r = s.find(new ImageTarget(imageUrl));
		
//		URL imageUrl = new URL("https://dl.dropbox.com/u/5104407/j.png");
//		List<ScreenRegion> rs = s.findAll(new ImageTarget(imageUrl));

		URL	imageUrl = new URL("http://code.google.com/images/code_logo.gif");
		ScreenRegion r = s.wait(new ImageTarget(imageUrl),10000);

		Mouse mouse = new RemoteMouse(remote);
		mouse.click(r.getCenter());
		
		Canvas canvas = new ScreenRegionCanvas(s);
		canvas.addBox(r).withLineColor(Color.green).display(3);
		//canvas.display(3);
		
		//ScreenRegion local = s.getRelativeScreenRegion(, yoffset, width, height)
		
//		ScreenPainter p = new ScreenPainter();
//		for (ScreenRegion r : rs){
//			p.box(r, 3000);
//		}
//		System.out.println(rs);
//		Mouse mouse = remote.getMouse();			
//		mouse.click(r.getCenter());
		

		
	}
}
