package org.sikuli.remote.examples;


import org.sikuli.remote.server.SikuliServer;

public class ServerExample {

	public static void main(String[] args) throws Exception {
	
		int port = 5000;		
			
		SikuliServer server = new SikuliServer(port);
		server.startup();
	}
}
