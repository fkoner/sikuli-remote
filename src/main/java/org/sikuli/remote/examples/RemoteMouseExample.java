package org.sikuli.remote.examples;

import java.net.URL;

import org.sikuli.api.ScreenRegion;
import org.sikuli.api.robot.Mouse;
import org.sikuli.remote.Remote;
import org.sikuli.remote.SikuliRemote;
import org.sikuli.remote.client.RemoteMouse;
import org.sikuli.remote.client.RemoteScreenRegion;

public class RemoteMouseExample {
	public static void main(String[] args) throws Exception {
		
		int port = 9000;	
		String host = "localhost";
							
		URL serverUrl = new URL("http://" + host + ":" + port + "/sikuli");
		
		Remote remote = new SikuliRemote(serverUrl);
		
		ScreenRegion s = new RemoteScreenRegion(remote);
		Mouse mouse = new RemoteMouse(remote);
		mouse.click(s.getRelativeScreenLocation(30,10));
		
	}
}
